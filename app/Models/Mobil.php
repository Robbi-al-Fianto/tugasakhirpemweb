<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Mobil extends Model
{
    use HasFactory;

    protected $table = 'mobil';
    protected $primaryKey = 'id_mobil';

    protected $fillable = [
        'merk_mobil',
        'jenis_mobil',
        'warna_mobil',
        'plat_mobil',
        'foto_mobil',
        'harga_sewa'
    ];

    public function transaksi()
    {
        return $this->hasMany(Transaksi::class, 'id_mobil', 'id_mobil');
    }

    public function uploadFotoMobil($file)
    {
        $filename = $file->getClientOriginalName();
        $file->storeAs('storage/fotomobil', $filename, 'public');

        $this->foto_mobil = $filename;
    }
}
