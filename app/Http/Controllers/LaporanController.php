<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Transaksi;

class LaporanController extends Controller
{
    public function index()
    {
        $transaksi = Transaksi::all();
        return view('laporan.index', compact('transaksi'));
    }

    public function filter(Request $request)
    {
        $dari = $request->input('dari');
        $sampai = $request->input('sampai');
        $transaksi = Transaksi::whereBetween('tanggal', [$dari, $sampai])->get();
        return view('laporan.index', compact('transaksi', 'dari', 'sampai'));
    }
}
