<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Mobil;
use Illuminate\Support\Facades\Storage;

class MobilController extends Controller
{
    public function index()
    {
        $mobil = Mobil::all();
        return view('mobil.index', compact('mobil'));
    }

    public function create()
    {
        return view('mobil.create');
    }

    public function store(Request $request)
    {
        $mobil = new Mobil($request->except('foto_mobil'));

        if ($request->hasFile('foto_mobil')) {
            $file = $request->file('foto_mobil');
            $filename = $file->getClientOriginalName();
            $file->storeAs('storage/fotomobil', $filename, 'public');
            $mobil->foto_mobil = $filename;
        }

        $mobil->save();

        return redirect()->route('mobil.index')->with('success', 'Data mobil berhasil ditambahkan');
    }

    public function edit($id)
    {
        $mobil = Mobil::find($id);
        return view('mobil.edit', compact('mobil'));
    }

    public function update(Request $request, $id)
    {
        $mobil = Mobil::find($id);
        $mobil->fill($request->except('foto_mobil'));

        if ($request->hasFile('foto_mobil')) {
            $file = $request->file('foto_mobil');
            $filename = $file->getClientOriginalName();
            $file->storeAs('storage/fotomobil', $filename, 'public');

            // Hapus foto mobil lama jika ada
            if ($mobil->foto_mobil) {
                Storage::disk('public')->delete('storage/fotomobil/' . $mobil->foto_mobil);
            }

            $mobil->foto_mobil = $filename;
        }

        $mobil->save();

        return redirect()->route('mobil.index')->with('success', 'Data mobil berhasil diupdate');
    }

    public function destroy($id)
    {
        $mobil = Mobil::find($id);

        // Hapus foto mobil jika ada
        if ($mobil->foto_mobil) {
            Storage::disk('public')->delete('storage/fotomobil/' . $mobil->foto_mobil);
        }

        $mobil->delete();

        return redirect()->route('mobil.index')->with('success', 'Data mobil berhasil dihapus');
    }
}
