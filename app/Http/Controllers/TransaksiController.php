<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Pelanggan;
use App\Models\Mobil;
use App\Models\Karyawan;
use App\Models\Transaksi;


class TransaksiController extends Controller
{
    public function index()
    {
        $transaksi = Transaksi::all();
        return view('transaksi.index', compact('transaksi'));
    }

    public function create()
    {
        $pelanggan = Pelanggan::all();
        $mobil = Mobil::all();
        $karyawan = Karyawan::all();

        return view('transaksi.create', compact('pelanggan', 'mobil', 'karyawan'));
    }

    public function store(Request $request)
    {
        Transaksi::create($request->all());
        return redirect()->route('transaksi.index')->with('success', 'Data transaksi berhasil ditambahkan');
    }

    public function edit($id)
    {
        $transaksi = Transaksi::find($id);
        $pelanggan = Pelanggan::all();
        $mobil = Mobil::all();
        $karyawan = Karyawan::all();

        return view('transaksi.edit', compact('pelanggan', 'mobil', 'karyawan', 'transaksi'));
    }

    public function update(Request $request, $id)
    {
        Transaksi::find($id)->update($request->all());
        return redirect()->route('transaksi.index')->with('success', 'Data transaksi berhasil diupdate');
    }

    public function destroy($id)
    {
        Transaksi::find($id)->delete();
        return redirect()->route('transaksi.index')->with('success', 'Data transaksi berhasil dihapus');
    }
}
