<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('transaksi', function (Blueprint $table) {
            $table->increments('id_transaksi');
            $table->unsignedInteger('id_pelanggan');
            $table->unsignedInteger('id_mobil');
            $table->unsignedInteger('id_karyawan');
            $table->date('tgl_pinjam');
            $table->date('tgl_kembali');
            $table->integer('harga_sewa');
            $table->integer('total_bayar');
            $table->timestamps();

            $table->foreign('id_pelanggan')->references('id_pelanggan')->on('pelanggan');
            $table->foreign('id_mobil')->references('id_mobil')->on('mobil');
            $table->foreign('id_karyawan')->references('id_karyawan')->on('karyawan');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('transaksi');
    }
};
