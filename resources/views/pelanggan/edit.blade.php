@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="d-flex justify-content-between align-items-center">
            <h2>Edit Data Pelanggan</h2>
            <a href="{{ route('pelanggan.index') }}" class="btn btn-danger">Kembali</a>
            </div>
            <form action="{{ route('pelanggan.update', $pelanggan->id_pelanggan) }}" method="POST">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label for="nama">Nama Pelanggan</label>
                    <input type="text" name="nama_pelanggan" id="nama_pelanggan" class="form-control" value="{{ $pelanggan->nama_pelanggan}}">
                </div>
                <div class="form-group">
                    <label for="alamat">Alamat</label>
                    <textarea name="alamat" id="alamat" class="form-control">{{ $pelanggan->alamat }}</textarea>
                </div>
                <div class="form-group">
                    <label for="no_telp">No. Telp</label>
                    <input type="number" name="no_telp" id="no_telp" class="form-control" value="{{ $pelanggan->no_telp }}">
                </div>
                  <div class="form-group">
                    <label for="no_ktp">No. KTP</label>
                    <input type="number" name="no_ktp" id="no_ktp" class="form-control" value="{{ $pelanggan->no_ktp }}">
                </div>
                <button type="submit" class="btn btn-primary">Simpan</button>
            </form>
        </div>
    </div>
</div>
@endsection