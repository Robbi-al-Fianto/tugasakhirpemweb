@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="d-flex justify-content-between align-items-center">
            <h2>Edit Transaksi</h2>
            <a href="{{ route('transaksi.index') }}" class="btn btn-danger">Kembali</a>
            </div>
            <form action="{{ route('transaksi.update', $transaksi->id_transaksi) }}" method="POST">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label for="id_pelanggan">Pelanggan</label>
                    <select name="id_pelanggan" id="id_pelanggan" class="form-control">
                        @foreach ($pelanggan as $data)
                            <option value="{{ $data->id_pelanggan }}" {{ $data->id_pelanggan == $transaksi->id_pelanggan ? 'selected' : '' }}>{{ $data->nama_pelanggan }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="id_mobil">Plat Mobil</label>
                    <select name="id_mobil" id="id_mobil" class="form-control" onchange="updateHargaSewa()">
                        @foreach ($mobil as $data)
                            <option value="{{ $data->id_mobil }}" {{ $data->id_mobil == $transaksi->id_mobil ? 'selected' : '' }} data-harga="{{ $data->harga_sewa }}">{{ $data->plat_mobil }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="id_karyawan">Karyawan</label>
                    <select name="id_karyawan" id="id_karyawan" class="form-control">
                        @foreach ($karyawan as $data)
                            <option value="{{ $data->id_karyawan }}" {{ $data->id_karyawan == $transaksi->id_karyawan ? 'selected' : '' }}>{{ $data->nama_karyawan }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="tgl_pinjam">Tanggal Pinjam</label>
                    <input type="date" name="tgl_pinjam" id="tgl_pinjam" class="form-control" value="{{ $transaksi->tgl_pinjam }}" onchange="calculateTotalBayar()">
                </div>
                <div class="form-group">
                    <label for="tgl_kembali">Tanggal Kembali</label>
                    <input type="date" name="tgl_kembali" id="tgl_kembali" class="form-control" value="{{ $transaksi->tgl_kembali }}" onchange="calculateTotalBayar()">
                </div>
                <div class="form-group">
                    <label for="harga_sewa">Harga Sewa</label>
                    <input type="number" name="harga_sewa" id="harga_sewa" class="form-control" value="{{ $transaksi->harga_sewa }}" readonly>
                </div>
                <div class="form-group">
                    <label for="total_bayar">Total Bayar</label>
                    <input type="number" name="total_bayar" id="total_bayar" class="form-control" value="{{ $transaksi->total_bayar }}" readonly>
                </div>
                <button type="submit" class="btn btn-primary">Simpan</button>
            </form>
        </div>
    </div>
</div>


<script>
    function updateHargaSewa() {
        const mobilSelect = document.getElementById('id_mobil');
        const hargaSewaElement = document.getElementById('harga_sewa');
        const platMobilSelect = document.getElementById('plat_mobil');

        const selectedOption = mobilSelect.options[mobilSelect.selectedIndex];
        const hargaSewa = parseFloat(selectedOption.getAttribute('data-harga'));
        const merkMobil = selectedOption.innerText;

        Array.from(platMobilSelect.options).forEach((option) => {
            option.style.display = option.getAttribute('data-merk') === merkMobil ? 'block' : 'none';
        });

        hargaSewaElement.value = hargaSewa;
    }

    function calculateTotalBayar() {
        const hargaSewaElement = document.getElementById('harga_sewa');
        const tglPinjamElement = document.getElementById('tgl_pinjam');
        const tglKembaliElement = document.getElementById('tgl_kembali');
        const totalBayarElement = document.getElementById('total_bayar');

        const mobilSelect = document.getElementById('id_mobil');
        const selectedOption = mobilSelect.options[mobilSelect.selectedIndex];
        const hargaSewa = parseFloat(selectedOption.getAttribute('data-harga'));

        const tglPinjam = new Date(tglPinjamElement.value);
        const tglKembali = new Date(tglKembaliElement.value);

        const diffTime = Math.abs(tglKembali - tglPinjam);
        const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
        const totalBayar = hargaSewa * diffDays;

        hargaSewaElement.value = hargaSewa;
        totalBayarElement.value = totalBayar;
    }
</script>

@endsection