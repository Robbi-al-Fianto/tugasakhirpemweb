@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="d-flex justify-content-between align-items-center">
                    <h2>Data Transaksi</h2>
                    <a href="{{ route('transaksi.create') }}" class="btn btn-primary">Tambah Data</a>
                </div>
                <table class="table mt-4">
                    <thead>
                        <tr>
                            <th scope="col">No</th>
                            <th scope="col">Pelanggan</th>
                            <th scope="col">Mobil</th>
                            <th scope="col">Plat Mobil</th>
                            <th scope="col">Karyawan</th>
                            <th scope="col">Tanggal Pinjam</th>
                            <th scope="col">Tanggal Kembali</th>
                            <th scope="col">Harga Sewa</th>
                            <th scope="col">Total</th>
                            <th scope="col">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($transaksi as $data)
                            <tr>
                                <th scope="row">{{ $loop->iteration }}</th>
                                <td>{{ $data->pelanggan->nama_pelanggan }}</td>
                                <td>{{ $data->mobil->merk_mobil }}</td>
                                <td>{{ $data->mobil->plat_mobil }}</td>
                                <td>{{ $data->karyawan->nama_karyawan }}</td>
                                <td>{{ $data->tgl_pinjam }}</td>
                                <td>{{ $data->tgl_kembali }}</td>
                                <td>{{ $data->harga_sewa }}</td>
                                <td>{{ $data->total_bayar }}</td>
                                <td>
                                    <a href="{{ route('transaksi.edit', $data->id_transaksi) }}" class="btn btn-warning">Edit</a>
                                    <form action="{{ route('transaksi.destroy', $data->id_transaksi) }}" method="POST" class="d-inline">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger">Hapus</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    @endsection