@extends('layouts.app')

@section('content')  
  <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="d-flex justify-content-between align-items-center">
                <h2>Tambah Mobil</h2>
                <a href="{{ route('mobil.index') }}" class="btn btn-danger">Kembali</a>
                </div>
                <form action="{{ route('mobil.store') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label for="merk_mobil">Merk Mobil</label>
                        <input type="text" class="form-control" name="merk_mobil" id="merk_mobil" required>
                    </div>
                    <div class="form-group">
                        <label for="jenis_mobil">Jenis Mobil</label>
                        <input type="text" class="form-control" name="jenis_mobil" id="jenis_mobil" required>
                    </div>
                    <div class="form-group">
                        <label for="warna_mobil">Warna Mobil</label>
                        <input type="text" class="form-control" name="warna_mobil" id="warna_mobil" required>
                    </div>
                     <div class="form-group">
                        <label for="plat_mobil">Plat Mobil</label>
                        <input type="text" class="form-control" name="plat_mobil" id="plat_mobil" required>
                    </div>
                    <div class="form-group">
                        <label for="foto_mobil">Foto Mobil</label>
                        <input type="file" class="form-control" name="foto_mobil" id="foto_mobil" required>
                    </div>
                    <div class="form-group">
                        <label for="harga_sewa">Harga Sewa</label>
                        <input type="number" class="form-control" name="harga_sewa" id="harga_sewa" required>
                    </div>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </form>
            </div>
        </div>
    </div>
@endsection
