@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="d-flex justify-content-between align-items-center">
                    <h2>Data Mobil</h2>
                    <a href="{{ route('mobil.create') }}" class="btn btn-primary">Tambah Data</a>
                </div>
                <table class="table mt-4">
                    <thead>
                        <tr>
                            <th scope="col">No</th>
                            <th scope="col">Merk Mobil</th>
                            <th scope="col">Jenis Mobil</th>
                            <th scope="col">Warna Mobil</th>
                            <th scope="col">Plat Nomor</th>
                            <th scope="col">Foto Mobil</th>
                            <th scope="col">Harga Sewa</th>
                            <th scope="col">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($mobil as $data)
                            <tr>
                                <th scope="row">{{ $loop->iteration }}</th>
                                <td>{{ $data->merk_mobil }}</td>
                                <td>{{ $data->jenis_mobil }}</td>
                                <td>{{ $data->warna_mobil }}</td>
                                <td>{{ $data->plat_mobil }}</td>
                                <td>
                                    @if ($data->foto_mobil)
                                        <img src="{{ asset('storage/fotomobil/' . $data->foto_mobil) }}" alt="Foto Mobil" width="100">
                                    @else
                                        <span>Tidak ada foto</span>
                                    @endif
                                </td>
                                <td>{{ $data->harga_sewa }}</td>
                                <td>
                                    <a href="{{ route('mobil.edit', $data->id_mobil) }}" class="btn btn-warning">Edit</a>
                                    <form action="{{ route('mobil.destroy', $data->id_mobil) }}" method="POST" class="d-inline">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger">Hapus</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection
