@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="d-flex justify-content-between align-items-center">
            <h2>Edit Data Karyawan</h2>
            <a href="{{ route('karyawan.index') }}" class="btn btn-danger">Kembali</a>
            </div>
            <form action="{{ route('karyawan.update', $karyawan->id_karyawan) }}" method="POST">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label for="nama">Nama Karyawan</label>
                    <input type="text" name="nama_karyawan" id="nama_karyawan" class="form-control" value="{{ $karyawan->nama_karyawan}}">
                </div>
                <div class="form-group">
                    <label for="alamat">Alamat</label>
                    <textarea name="alamat" id="alamat" class="form-control">{{ $karyawan->alamat }}</textarea>
                </div>
                <div class="form-group">
                    <label for="no_telp">No. Telp</label>
                    <input type="number" name="no_telp" id="no_telp" class="form-control" value="{{ $karyawan->no_telp }}">
                </div>
                <div class="form-group">
                    <label for="jabatan">No. Telp</label>
                    <input type="text" name="jabatan" id="jabatan" class="form-control" value="{{ $karyawan->jabatan}}">
                </div>
                <button type="submit" class="btn btn-primary">Simpan</button>
            </form>
        </div>
    </div>
</div>

@endsection